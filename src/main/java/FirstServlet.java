import velykyi.vladyslav.Cart;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class FirstServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        HttpSession session = request.getSession();

        // - - - - Authorization - - - -
//        String user = (String) session.getAttribute("current_user");
//
//        if (user == null){
            //response для анонімного користувача
            //авторизація
            //реєстрація
            //session.setAttribute("current_user", ID);
//        }else{
               //response для авторизованого користувача
//        }

        // - - - - Cart for user with forward to showCart.jsp - - - -
        Cart cart = (Cart) session.getAttribute("cart");

        String name = request.getParameter("name");
        int quantity =  Integer.parseInt(request.getParameter("quantity"));

        if(cart == null){
            cart = new Cart();

            cart.setName(name);
            cart.setQuantity(quantity);
        }
        session.setAttribute("cart", cart);
        getServletContext().getRequestDispatcher("/showCart.jsp").forward(request, response);




// - - - - - - Counter of visiting page for user - - - - - -
//        Integer count = (Integer) session.getAttribute("count");
//
//
//        if (count == null) {
//            count = 1;
//            session.setAttribute("count", count);
//        } else
//            session.setAttribute("count", count + 1);
//
//        String name = request.getParameter("name");
//        String surname = request.getParameter("surname");
//        PrintWriter pw = response.getWriter();
//        pw.println("<html>");
//        pw.println("<h1> Your count is: " + count + "</h1>");
//        //pw.println("<h1>" + "Hello " + name + " " + surname + " </h1>");
//        pw.println("</html>");

        // response.sendRedirect("https://www.google.com");
        // response.sendRedirect("/testJSP.jsp");
        // RequestDispatcher dispatcher = request.getRequestDispatcher("/testJSP.jsp");
        // dispatcher.forward(request, response);
    }
}
